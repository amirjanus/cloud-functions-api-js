'use strict';

const functions = require( "firebase-functions" );
const admin = require( "firebase-admin" );

let serviceAccount = require( "./my-functions-80360-firebase-adminsdk-w0nz0-67309a6963" );

admin.initializeApp( {
    credential: admin.credential.cert( serviceAccount ),
    databaseURL: "https://my-functions-80360.firebaseio.com"
} );

let db = admin.firestore();
let auth = admin.auth();

let express = require( "express" );
let app = express();

let cors = require( "cors" );

app.use( cors( { origin: true } ) );

const movieRoutes = require( "./movies/routes" );
movieRoutes.SetFirestore( db );
movieRoutes.SetAuth( auth );

app.use( "/", movieRoutes.router );

exports.movies = functions.https.onRequest( app );
