const express = require( "express" );

let router = express.Router();

let db;

let SetFirestore = function( database ) {
    db = database;
};

let auth;

let SetAuth = function( authentication ) {
    auth = authentication;
};

/**
 * Gets single document by ID.
 *
 * @param id ( required ): Document ID.
 */
router.get( "/", ( req, res, next ) => {
    let id = req.query.id;

    if ( id != null ) {
        db.collection( "movies" ).doc( id ).get()
            .then( documentSnapshot => {
                if ( documentSnapshot.exists ) {

                    let data = documentSnapshot.data();
                    data.response = true;

                    res.json( data );

                } else {
                    res.json( { response: false, error: "Document does not exist" } );
                }

            } )
            .catch( error => res.json( { response: false, error: "Error getting document" } ) );
    } else {
        next();
    }
} );

/**
 * Gets multiple document in collection.
 *
 * @param range ( required ): Number of documents to return.
 * @param page ( required ): Page number to return. Start value is 1.
 */
router.get( "/", ( req, res ) => {
    if ( req.query.range == null ) {
        res.json( { response: false, error: "Invalid query parameters" } );
        return;
    }

    if ( req.query.page == null ) {
        res.json( { response: false, error: "Invalid query parameters" } );
        return;
    }

    const range = Number( req.query.range );
    const page = Number( req.query.page );

    let collectionRef = db.collection( "movies" );

    collectionRef.get()
        .then( querySnapshot => {
            let items = [];

            const startAt = ( page - 1 ) * range;
            const endAt = Math.min( startAt + range - 1, querySnapshot.size - 1 );

            for ( let i = startAt; i <= endAt; ++i ) {

                const documentSnapshot = querySnapshot.docs[ i ];

                const movie = documentSnapshot.data();
                movie.id = documentSnapshot.id;

                items.push( movie );

            }

            let result = {};

            if ( startAt > querySnapshot.size ) {

                result.response = false;
                result.error = "Out of bounds.";

            } else {

                result.response = true;
                result.list = items;
                result.totalResults = querySnapshot.size;

            }

            res.json( result );
        } )
        .catch( error =>
            res.json( { response: false, error: "Error getting document" } ) );
} );

/**
 * Adds new item to collection.
 *
 * @param body.data: Movie data object to add to Firestore.
 * @param body.token: Object with Firebase Auth ID token.
 */
router.post( "/", ( req, res ) => {
    const data = req.body.data;

    if ( data == null ) {
        res.json( { response: false, error: "Invalid body parameters: data" } );
        return;
    }

    const token = req.body.token.idToken;

    if ( token == null ) {
        res.json( { response: false, error: "Invalid body parameters: idToken" } );
        return;
    }

    auth.verifyIdToken( token, true )
        .then( decodedToken =>
            db.collection( "movies" ).add( data ) )
        .then( documentReference =>
            res.json( { id: documentReference.id, response: true } ) )
        .catch( error =>
            res.json( { response: false, error: "Error creating document" } ) );
} );

/**
 * Updates single document.
 *
 * @param id ( required ): ID of a document to update.
 * @param body.data: Object with movie data to update.
 * @param body.token: Object with Firebase Auth ID token.
 */
router.put( "/", async ( req, res ) => {
    const id = req.query.id;

    if ( id == null ) {
        res.json( { response: false, error: "Invalid query parameters: id" } );
        return;
    }

    const data = req.body.data;

    if ( data == null ) {
        res.json( { response: false, error: "Invalid body parameters: data" } );
        return;
    }

    const token = req.body.token.idToken;

    if ( token == null ) {
        res.json( { response: false, error: "Invalid body parameters: idToken" } );
        return;
    }

    if ( !( Object.keys( data ) ).length ) {
        res.json( { response: false, error: "Update object empty." } );
        return;
    }

    auth.verifyIdToken( token, true )
        .then( decodedToken =>
            db.collection( "movies" ).doc( id ).update( data )
        )
        .then( writeResult =>
            res.json( { response: true, id: id } )
        )
        .catch( error =>
            res.json( { response: false, error: "Error updating document" } )
        );
} );

/**
 * Deletes single document.
 *
 * @param id ( required ): ID of a document to delete.
 * @param body.token: Object with Firebase Auth ID token.
 */
router.delete( "/", async ( req, res ) => {
    const id = req.query.id;

    if ( id == null ) {
        res.json( { response: false, error: "Invalid query parameters: id" } );
        return;
    }

    const token = req.body.idToken;

    if ( token == null ) {
        res.json( { response: false, error: "Invalid body parameters: idToken" } );
        return;
    }

    auth.verifyIdToken( token, true )
        .then( decodedToken =>
            db.collection( "movies" ).doc( id ).delete()
        )
        .then( () =>
            res.json( { id: id, response: true } )
        )
        .catch( error =>
            res.json( { response: false, error: "Error deleting document" } )
        );
} );

module.exports = {
    router,
    SetFirestore,
    SetAuth
};
